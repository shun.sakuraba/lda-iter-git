/* 
Copyright (c) 2013 Shun Sakuraba

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#include <Eigen/Dense>
#include <algorithm>
#include <vector>

#include <cassert>

using namespace std;
using namespace Eigen;

void scatterMatrix(const vector<MatrixXd> &m,
                   const vector<double> &withinScatterWeights,
                   MatrixXd &withinClass, MatrixXd &betweenClass,
		   MatrixXd &centroids, VectorXd &wholecentroid)
{
  size_t classes = m.size();
  assert(!m.empty());
  size_t N = m[0].rows();

  centroids.setZero(N, classes);
  wholecentroid.setZero(N);
  
  withinClass.setZero(N, N);
  betweenClass.setZero(N, N);
  
  size_t totalT = 0;
  for(size_t i = 0; i < classes; ++i) {
    centroids.col(i) = m[i].rowwise().mean();
    wholecentroid += centroids.col(i);
  }
  wholecentroid *= (1. / classes);
  
  size_t block = 600;
  for(size_t i = 0; i < classes; ++i) {
    // set in-class matrix
    const MatrixXd &cur = m[i];
    MatrixXd buf(N, block);

    for(size_t t = 0; t < cur.cols(); t += block) {
      size_t blocksize = std::min(block, (size_t)(cur.cols() - t));
      
      buf.block(0, 0, N, blocksize) = cur.block(0, t, N, blocksize);
      buf.block(0, 0, N, blocksize).colwise() -= centroids.col(i);
      buf.block(0, 0, N, blocksize) *= (1.0 / sqrt((double)cur.cols()));
      
      withinClass += withinScatterWeights[i] * buf.block(0, 0, N, blocksize) * buf.block(0, 0, N, blocksize).transpose();
    }

    // set between-class matrix
    {
      VectorXd dv = centroids.col(i) - wholecentroid;
      betweenClass += dv * dv.transpose();
    }
  }

  return;
}
  
