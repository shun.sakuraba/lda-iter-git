CXX = g++
CC = gcc

EIGENDIR = /usr/include/eigen3

CFLAGS = -g -O2 -march=native
CXXFLAGS = -g -O2 -march=native -I$(EIGENDIR)
LDFLAGS = 
LIBS = -ldl

# This turns program to use EVD in PCA instead of SVD. It's recommended because Eigen's SVD is very slow (even compared to dgesvj in reference LAPACK). 
CXXFLAGS += -DUSE_EVD_PCA

all: lda

clean:
	rm -rf *.o lda

check: traceRatioOptTest
	./traceRatioOptTest

traceRatioOptTest: traceRatioOptTest.o traceRatioOpt.o scatterMatrix.o
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

lda: main.o pdb.o scatterMatrix.o traceRatioOpt.o vmdfio/vmdfio.o
	$(CXX) $(LDFLAGS) -o $@ $^ $(LIBS)

main.o: bestfit/bestfit.hpp traceRatioOpt.hpp scatterMatrix.hpp

%.o: %.cpp
	$(CXX) $(CXXFLAGS) -c -o $@ $<

%.o: %.c
	$(CC) $(CFLAGS) -c -o $@ $<


