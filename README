Thank you for downloading LDA-ITER implementation for MD analysis (I call this software "lda").
Read the following instructions to use lda:

1. Compilation

To compile this program you need a C/C++ compiler and Eigen3 ( http://eigen.tuxfamily.org/ ) library. 
Modify EIGENDIR variable in "Makefile" to correct one. Also modify compiler and its flags to your environment.

Then, just make:
$ make

You will obtain an executable named "lda".

2. Preparing input files
The lda requires MD trajectories to use. The periodicity should be fixed so that all target molecules are continuous in each snapshot (no need to be continuous in time though). GROMACS users usually have to fix periodicity first because "mdrun" program usually output trajectories without wrapping molecules by periodicity. Note that users do not have to best-fit the input structure (lda does that internally).

You also need "mask" PDB files, matching to the topology of trajectories. The "B-factor" column must be equal to the mass of the atom if the atom is selected for comparison, or zero otherwise. For example,

ATOM     10  CB  PRO X   1      40.300  66.970  27.650  0.00  0.00            
ATOM     11  HB1 PRO X   1      39.870  66.520  28.430  0.00  0.00            
ATOM     12  HB2 PRO X   1      40.390  67.950  27.830  0.00  0.00            
ATOM     13  CA  PRO X   1      41.650  66.330  27.320  0.00 12.00            
ATOM     14  HA  PRO X   1      42.360  67.030  27.210  0.00  0.00            
ATOM     15  C   PRO X   1      42.080  65.390  28.440  0.00  0.00            

selects C-alpha atom of Proline in the trajectory and neglects others. The number of non-zero B-factor values should be the same among mask PDB files. The non-zero B-factor values itself also be the same among PDB files. (Currently we do not support permutation of atoms.)

The snapshots in trajectories are best-fit to this PDB file before analyzing the data.

3. Running software

Lda uses VMD's molfile plugins, developed at University of Illinois, to access trajectory. Typically these files are placed under "(path to VMD's installation)/lib/plugins/(your machine architecture)/molfile". In my own computer, for example, it is placed under /opt/vmd/lib/plugins/LINUXAMD64/molfile:

$ ls /opt/vmd/lib/plugins/LINUXAMD64/molfile
abinitplugin.so    dsn6plugin.so     moldenplugin.so    stlplugin.so
avsplugin.so       dtrplugin.so      msmsplugin.so      tinkerplugin.so
babelplugin.so     dxplugin.so       namdbinplugin.so   uhbdplugin.so
basissetplugin.so  edmplugin.so      netcdfplugin.so    vaspchgcarplugin.so
bgfplugin.so       fs4plugin.so      offplugin.so       vaspoutcarplugin.so
binposplugin.so    gamessplugin.so   parm7plugin.so     vaspparchgplugin.so
biomoccaplugin.so  graspplugin.so    parmplugin.so      vaspposcarplugin.so
brixplugin.so      grdplugin.so      pbeqplugin.so      vaspxdatcarplugin.so
carplugin.so       gridplugin.so     pdbplugin.so       vaspxmlplugin.so
ccp4plugin.so      gromacsplugin.so  phiplugin.so       vtfplugin.so
corplugin.so       hoomdplugin.so    pltplugin.so       webpdbplugin.so
cpmdplugin.so      jsplugin.so       pqrplugin.so       xbgfplugin.so
crdplugin.so       lammpsplugin.so   psfplugin.so       xsfplugin.so
cubeplugin.so      maeffplugin.so    raster3dplugin.so  xyzplugin.so
dcdplugin.so       mapplugin.so      rst7plugin.so
dlpolyplugin.so    mdfplugin.so      situsplugin.so
dmsplugin.so       mol2plugin.so     spiderplugin.so
$

Then, set environment variable VMD_PLUGINS to this path. Depending your shell type, this part separates into two ways:

bash/zsh:
$ export VMD_PLUGINS=/opt/vmd/lib/plugins/LINUXAMD64/molfile

csh/tcsh:
$ setenv VMD_PLUGINS /opt/vmd/lib/plugins/LINUXAMD64/molfile

Then, execute "lda".
$ ./lda 2 /path/to/wt-simulation-mask.pdb /path/to/wt/trajectory1.xtc /path/to/wt/trajectory2.xtc /path/to/mutant/mutant-simulation-mask.pdb /path/to/mutant/trajectory.xtc

The first command-line argument is the number of dimensions to extract. In Ref. [1] we suggested to use dimensions of 2 for visualization purpose.

The second argument is the "mask" pdb file prepared on section 2. 

Third and fourth arguments are the trajectory files.  You can specify multiple trajectory files for one mask file. The number of atoms in mask PDB file should match the number of atoms in trajectory files (otherwise there will be an error).

The fifth argument is another "mask" pdb file, representing the "different" condition from the first mask's. The sixth argument is the trajectory corresponding to the second mask file.

3.1. Special notes for AMBER
The program determines trajectory's file type depending on its file extension. Thus, if the file extension is not properly set, the program may not access the trajectory file. For NAMD/GROMACS/CHARMM users this will not be problem, since default trajectory file extensions is fixed (.dcd, .trr, or .xtc). For AMBER users using binary trajectory format, the user must set the name of the trajectory to .nc (NetCDF). If your trajectory is not named as .nc, make symbolic links to work around.
$ ln -s trajectory_amber.trj trajectory_amber.nc

4. Output files

After execution the program creates following files:
proj_pca.txt
proj_lda.txt
vec_pca.txt
vec_lda.txt
meanstructure.txt
(only when there are two-classes) proj_direct.txt

4.1. Projections
proj_*.txt files represent the projected value to the PCA/LDA-ITER vectors. Each line represent the class number and the projections. An example from two-dimension extractions:
...
0 -6.5379168682990922e+01 1.9905878918435707e+00
0 -6.5137764090504646e+01 9.9103407795378973e-01
0 -6.7395358260064569e+01 1.3909901135623499e+00
0 -6.9786815053988093e+01 2.1761940340166053e+00
1 7.4214282062031117e+01 -1.8214190851578367e+00
1 7.0651604961944116e+01 -1.5912398109181454e+00
...

The first column represent the class number (0-origin). This corresponds to the index of mask PDB file.
For examle, if the command-line is

$ ./lda 2 /path/to/wt-simulation-mask.pdb /path/to/wt/trajectory1.xtc /path/to/wt/trajectory2.xtc /path/to/mutant/mutant-simulation-mask.pdb /path/to/mutant/trajectory.xtc

The 0th class corresponds to the topology of wt-simulation-mask while 1st class corresopnds to that of mutant-simulation-mask.

The remaining values represent the projected values (left to right in order.)

The number of lines in the output files shold match the total number of snapshots in trajectories (in order).

4.2. Vectors
vec_*.txt files represent the vectors of PCA/LDA. The vectors are represented as follows:

  0.0208844  -0.0130773   0.0204367 -0.00164139 -0.00226054 -0.00204543
  -0.0403011 -0.000894161   -0.0311508   0.00422232   0.00351329   0.00574349
 -0.0194797  0.00422614     0.01148 -0.00316353  0.00163359 -0.00299038
 -0.0134878   0.0535453   0.0548751   0.0026798  -0.0071763 -0.00402009
 ...

Each line contains (3 * dimensions) values. The values on the first three columns correspond to the first PCA/LDA vector, i.e. each line represents x1, y1, z1, x2, y2, z2, ... .
Each line corresponds to the atoms with non-zero B-factor values in mask PDBs.

4.3. Misc

meanstructure.txt represents the global mean (fitted to the mask PDB file).
proj_direct.txt represents the projection to the difference of average structure.

5. Bugs and bug reports
Current implementation loads all snapshots into memory, and thus it may not work on large dataset. Try to run on a machine with large memory.

Current implementation uses eigendecomposition in Eigen3, which is an order of magnitude slower than the state-of-art implementation of LAPACK. Please DO NOT use this program for benchmarking.

For bugs, contact shun dot sakuraba at GMail.

6. References

[1] S. Sakuraba and H. Kono. Spotting the difference in molecular dynamics simulations of biomolecules. Submitted.

