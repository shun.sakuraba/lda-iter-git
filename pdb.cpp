/* 
Copyright (c) 2013 Shun Sakuraba

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#include "pdb.hpp"
#include <fstream>

using namespace std;
using namespace Eigen;

bool loadPDB(const string &fname,
             Matrix3Xd &crds,
             VectorXd &betas)
{
  ifstream ifs(fname.c_str());
  
  string line;

  vector<Vector3d> tmpcrds;
  vector<double> tmpbetas;

  while(getline(ifs, line)) {
    if(line.empty()) continue;
    if(line.size() < 6) continue;
    string cmd = line.substr(0, 6);
    if(cmd == "ATOM  " || cmd == "HETATM") {
      string s = line.substr(30, 24);
      double x, y, z;
      int r;
      r = sscanf(s.c_str(), "%8lf%8lf%8lf", &x, &y, &z);
      if(r != 3) return false;
      s = line.substr(60, 8);
      double beta = 0.;
      r = sscanf(s.c_str(), "%6lf", &beta);
      if(r != 1) return false;
      tmpcrds.push_back(Vector3d(x, y, z));
      tmpbetas.push_back(beta);
    }
  }
  
  size_t cols = tmpcrds.size();
  crds = Matrix3Xd(3, cols);
  betas = VectorXd(cols);
  
  for(size_t i = 0; i < cols; ++i) {
    crds.col(i) = tmpcrds[i];
    betas(i) = tmpbetas[i];
  }
  return true;
}

