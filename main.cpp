/* 
Copyright (c) 2013-2016 Shun Sakuraba

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#include "bestfit/bestfit.hpp"
#include "vmdfio/vmdfio.h"
#include "pdb.hpp"
#include "scatterMatrix.hpp"
#include "traceRatioOpt.hpp"
#include <cmath>
#include <cstdlib>
#include <string>
#include <vector>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <Eigen/Dense>
#include <Eigen/Sparse>
#include <Eigen/SVD>

using namespace std;
using namespace Eigen;

string extension(const string& s)
{
  size_t pos = s.find_last_of('.');
  if(pos == string::npos) {
    return "";
  }
  
  return s.substr(pos + 1);
}

// Meh, I packed everything into the main()...
int main(int argc, char *argv[])
{
  if(argc <= 4) {
    cerr << "Usage: " << argv[0] << " Ndim a.pdb a.xtc b.pdb b.xtc b2.xtc" << endl;
    return EXIT_FAILURE;
  }

  vmdfio_init_();

  vector<Matrix3Xd> refstrs;
  vector<MatrixXd> xs;
  vector<bool> mask;
  VectorXd mass;
  size_t nmasked;
  int ndim;
  double within_scatter_weight = 0.0;
  vector<double> within_scatter_weights;
  sscanf(argv[1], "%d", &ndim);
  for(int arg = 2; arg < argc; ++arg) {
    string f = argv[arg];
    if(extension(f) == "pdb") {
      // Load PDB
      VectorXd betas;
      Matrix3Xd crds;
      bool ok = loadPDB(f, crds, betas);
      if(!ok) {
        cerr << "Error: failed to load \"" << f << "\"" << endl;
        return EXIT_FAILURE;
      }
      mask.clear();
      nmasked = 0;
      for(size_t i = 0; i < betas.rows(); ++i) {
        mask.push_back(betas(i) > 0.0);
        nmasked += (betas(i) > 0.0);
      }
      cerr << "Read " << betas.rows() << " atoms ("
           << nmasked << " atoms enabled)" << endl;
      refstrs.push_back(Matrix3Xd());
      Matrix3Xd &ref = refstrs.back();
      ref.resize(3, nmasked);
      mass.resize(nmasked);
      size_t c = 0;
      for(size_t i = 0; i < crds.cols(); ++i) {
        if(betas[i] > 0.0) {
          ref.col(c) = crds.col(i);
          mass(c) = betas(i);
          ++c;
        }
      }
      xs.push_back(MatrixXd(nmasked * 3, 0));
      within_scatter_weights.push_back(within_scatter_weight);
      within_scatter_weight = 1.0;
    }else if(f[0] == '-') {
      if(f == "--forget-this-within-scatter") {
        within_scatter_weight = 0.0;
      }else{
        cerr << "Unknown option: \"" << f << "\"" << endl;
        return EXIT_FAILURE;
      }
    }else{
      if(mask.empty()) {
        cerr << "Error: mask pdb file must be specified before trajectory \"" << f << "\"" << endl;
        return EXIT_FAILURE;
      }
      // Load trajectory
      void* vmdh;
      int fnamelen = f.length();
      int status;
      char* forcetype;
      forcetype = reinterpret_cast<char*>(getenv("VMD_PLUGINS_FORCE_TYPE"));
      vmdfio_open_(&vmdh, f.c_str(), &fnamelen, forcetype, &status);
      if(status != 0) {
        cerr << "Error: failed to load \"" << f << "\"" << endl;
        return EXIT_FAILURE;
      }
      int natoms;
      vmdfio_get_natoms_(&vmdh, &natoms);
      if(natoms == -1) natoms = mask.size();
      if(natoms != mask.size()) {
        cerr << "Error: no. of atoms in \"" << f << "\"mismatch to PDB" << endl;
        return EXIT_FAILURE;
      }
      
      vector<VectorXd> traj_tmp;
      vector<double> buf(natoms * 3);
      vector<double> box(9);
      int natoms_aux = mask.size();
      if(f.length() > 4 
          && (f.substr(f.length() - 4, std::string::npos) == ".xtc" ||
              f.substr(f.length() - 4, std::string::npos) == ".trr" ||
              f.substr(f.length() - 4, std::string::npos) == ".tng")) {
        cerr << "(Removing initial frame from trajectory " << f << " to prevent duplication)" << endl;
        vmdfio_read_traj_step_(&vmdh, &buf[0], &box[0], &natoms_aux, 
                               &status);
        if(status != 0) {
            cerr << "Error: failed to read a frame from " << f << endl;
        }
      }
      while(true) {
        vmdfio_read_traj_step_(&vmdh, &buf[0], &box[0], &natoms_aux, 
                               &status);
        if(status == 0) {
          VectorXd tmp(3 * nmasked);
          size_t c = 0;
          for(size_t i = 0; i < mask.size(); ++i) {
            if(mask[i]) {
              tmp(c * 3) = buf[i * 3];
              tmp(c * 3 + 1) = buf[i * 3 + 1];
              tmp(c * 3 + 2) = buf[i * 3 + 2];
              ++c;
            }
          }
          traj_tmp.push_back(tmp);
        }else{
          MatrixXd &x = xs.back();
          size_t curx = x.cols();
          x.conservativeResize(NoChange, curx + traj_tmp.size());
          for(size_t i = 0; i < traj_tmp.size(); ++i){
            x.col(curx + i) = traj_tmp[i];
          }
          cerr << "Read " << traj_tmp.size() << " frames (current total: " << x.cols() << ")" << endl;
          break;
        }
      }
    }
  }
  
  vmdfio_fini_();

  within_scatter_weights.push_back(within_scatter_weight);
  within_scatter_weights.erase(within_scatter_weights.begin());
  cerr << "Within-scatter weights:";
  for(size_t i = 0; i < within_scatter_weights.size(); ++i) {
    cerr << " " << within_scatter_weights[i];
  }
  cerr << endl;

  // fit reference structure to the first one
  for(size_t i = 1; i < xs.size(); ++i) {
    bestfit(refstrs[0], mass, refstrs[i]);
  }
  
  // fit each trajctory to reference
  for(size_t i = 0; i < xs.size(); ++i) {
    bestfit_all(refstrs[i], mass, xs[i]);
  }

  {
    VectorXd mass3(nmasked * 3);
    for(size_t i = 0; i < nmasked; ++i) {
      mass3.segment(i * 3, 3) = VectorXd::Constant(3, mass(i));
    }
    
    for(size_t i = 0; i < xs.size(); ++i) {
      MatrixXd c = mass3.array().sqrt().matrix().asDiagonal() * xs[i];
      xs[i].swap(c);
    }
  }
    
  size_t ndegfreedom = 3 * nmasked;
  VectorXd unitmass(VectorXd::Constant(nmasked, 1.0));
  
  double rmsd_prev = 1e10;
  Matrix3Xd centroid;
  while(true){
    // compute temporal centroid
    centroid.setZero(3, nmasked);
    VectorXd e(ndegfreedom);
    for(size_t i = 0; i < xs.size(); ++i) {
      VectorXd e = xs[i].rowwise().mean();
      centroid += Map<Matrix3Xd>(e.data(), 3, nmasked);
    }
    centroid /= xs.size();

    // fit all to temporal centroid until convergent
    for(size_t i = 0; i < xs.size(); ++i) {
      bestfit_all(centroid, unitmass, xs[i]);
    }

    // calculate rmsd
    double rmsd = 0.;
    size_t ntot = 0;
    vector<double> rmsds(xs.size());
    for(size_t i = 0; i < xs.size(); ++i) {
      MatrixXd &x = xs[i];
      double rmsd_i = 0.;
      for(size_t j = 0; j < x.cols(); ++j) {
        double sd = 0.;
        for(size_t k = 0; k < nmasked; ++k) {
          for(size_t l = 0; l < 3; ++l) {
            double d = x(l + k * 3, j) - centroid(l, k);
            sd += d * d;
          }
        }
        rmsd_i += sd;
      }
      ntot += x.cols();
      rmsds[i] = sqrt(rmsd_i / (x.cols() * mass.array().sum()));
      rmsd += rmsd_i;
    }
    rmsd /= (double)ntot * mass.array().sum();
    rmsd = sqrt(rmsd);
    
    cerr << "Rmsd = " << rmsd << endl;
    if(rmsd >= rmsd_prev) {
      cerr << "Rmsds from sets: " << endl;
      for(size_t i = 0; i < xs.size(); ++i) {
        cerr << " " << i << " " << rmsds[i] << endl;
      }
      cerr << "Convergence confirmed, start LDA" << endl;
      break;
    }
    rmsd_prev = rmsd;
  }

  {
    // Output average structure
    ofstream res("meanstructure.txt");

    for(size_t i = 0; i < centroid.cols(); ++i) {
      res.setf(ios::scientific);
      res << setprecision(16);
      res << centroid.col(i).transpose() << endl;
    }
  }

  // Perform PCA to remove degrees of freedom
  vector<MatrixXd> xs_pca;
  MatrixXd projmat;
  VectorXd centroidx;
  size_t nselected;
  { 
    // Computation of centroid, in PCA manner
    centroidx.setZero(ndegfreedom);
    size_t totcols = 0.;
    for(size_t c = 0; c < xs.size(); ++c) {
      centroidx += xs[c].rowwise().sum();
      totcols += xs[c].cols();
    }
    centroidx /= totcols;

    MatrixXd wholedata(centroidx.rows(), totcols);
    size_t pos = 0;
    for(size_t c = 0; c < xs.size(); ++c) {
      MatrixXd &x = xs[c];
      wholedata.block(0, pos, x.rows(), x.cols()) = x.colwise() - centroidx;
      pos += x.cols();
    }

    cerr << "Performing PLS-DA for comparison ..." << endl;
    // PLS-DA analysis here, because shifted coordinates are used. PLS is performed by Helland's method. 
    if(xs.size() == 2){
      vector<VectorXd> projvecs(ndim);
      MatrixXd t(totcols, ndim);
      VectorXd plsalign(totcols);
      VectorXd plsremain(totcols);

      plsalign.head(xs[0].cols()) = VectorXd::Constant(xs[0].cols(), -1.0);
      plsalign.tail(xs[1].cols()) = VectorXd::Constant(xs[1].cols(), 1.0);
      plsremain = plsalign;

      for(size_t i = 0; i < ndim; ++i) {
        projvecs[i] = wholedata * plsremain; // of dimension ndegfreedom
        t.col(i) = wholedata.transpose() * projvecs[i];
        MatrixXd t_tmp = t.block(0, 0, totcols, (i + 1)); // of totcols * (i+1)
        MatrixXd t2 = t_tmp.transpose() * t_tmp; // of (i+1) * (i+1)
        FullPivLU<MatrixXd> solver(t2);
        plsremain = plsalign - t_tmp * solver.solve(t_tmp.transpose() * plsalign);
      }

      // Normalize vectors.
      for(size_t i = 0; i < ndim; ++i) {
        projvecs[i] = projvecs[i].normalized();
      }

      {
        ofstream res("vec_pls.txt");
        for(size_t i = 0; i < nmasked; ++i) {
          for(size_t j = 0; j < ndim; ++j) {
            res << projvecs[j].segment(i * 3, 3).transpose() << " ";
          }
          res << endl;
        }
      }
      
      {
        ofstream res("proj_pls.txt");
        size_t p = 0;
        for(size_t c = 0; c < xs.size(); ++c) {
          MatrixXd &x = xs[c];
          for(size_t i = 0; i < x.cols(); ++i) {
            res << c;
            for(size_t j = 0; j < ndim; ++j) {
              res << " " << wholedata.col(p).dot(projvecs[j]);
            }
            res << endl;
            ++p;
          }
        }
      }
    }
    

    cerr << "Performing EVD/SVD to remove 6+ degrees of freedom (This may take some time)" << endl;
    
    {
      // TODO: prepare LAPACK SVD (QR-based or MRRR) version
#ifdef USE_EVD_PCA
      // Uses EVD of AA^T. Slightly lower precision. Usually this drop in precision does not matter.
      {
        MatrixXd smat;
        smat = wholedata * wholedata.transpose();
        // Release memory
        { MatrixXd tmp; wholedata.swap(tmp); }
        
        SelfAdjointEigenSolver<MatrixXd> solver(smat);
        VectorXd evs = solver.eigenvalues();

        const double eps = 1.e-6;
        size_t zero_end;
        for(zero_end = 0;
            zero_end < evs.rows();
            ++zero_end) {
          if(evs(zero_end) > eps)
            break;
        }
        if(zero_end >= evs.rows()) {
          cerr << "Error: all eigenvalues are zero!" << endl;
          return EXIT_FAILURE;
        }
        nselected = evs.rows() - zero_end;
        cerr << "Found " << nselected << " non-null-space dimensions in " << ndegfreedom << " dimensions" << endl;
        cerr << "(Note: the smallest non-zero eigenvalue is " << evs(zero_end) << ")" << endl;
        projmat.resize(ndegfreedom, nselected);
        for(int c = 0; c < nselected; ++c) {
          projmat.col(c) = solver.eigenvectors().col(evs.rows() - 1 - c);
        }
      }
#else
      {
        JacobiSVD<MatrixXd> svd(wholedata, ComputeFullU);
        // Decreasing order in singular value
        VectorXd svs = svd.singularValues();
        const double eps = 1.e-6;
        size_t zero_start;
        for(zero_start = 0;
            zero_start < svs.rows();
            ++zero_start) {
          if(svs(zero_start) < eps)
            break;
        }
        if(zero_start == 0) {
          cerr << "Error: all singular values are zero!" << endl;
          return EXIT_FAILURE;
        }
        nselected = zero_start;
        cerr << "Found " << nselected << " non-null-space dimensions in " << ndegfreedom << " dimensions" << endl;
        cerr << "(Note: the smallest non-zero singular value is " << svs(zero_start - 1) << ")" << endl;
        projmat = svd.matrixU().block(0, 0, ndegfreedom, nselected);

        { MatrixXd tmp; wholedata.swap(tmp); }
      }
#endif
      
      cerr << "Converting into new coordinate system" << endl;
      size_t cur = 0;
      for(size_t c = 0; c < xs.size(); ++c){
        xs_pca.push_back(projmat.transpose() * (xs[c].colwise() - centroidx).matrix());
      }
    }
  }

  // purge xs (in C++03)
  {
    vector<MatrixXd> tmp;
    xs.swap(tmp);
  }

  cerr << "Writing PCA results" << endl;
  { 
    // Output PCA results
    ofstream res("proj_pca.txt");
    
    for(size_t c = 0; c < xs_pca.size(); ++c) {
      MatrixXd &x = xs_pca[c];
      VectorXd proj;
      for(size_t i = 0; i < x.cols(); ++i) {
        res.setf(ios::scientific);
        res << setprecision(16);
        res << c << " " << x.col(i).head(ndim).transpose() << endl;
      }
    }
  }

  {
    cerr << "Computing scatter matrices" << endl;
    // Start LDA
    MatrixXd withinClass;
    MatrixXd betweenClass;
    MatrixXd centroids;
    VectorXd wholecentroid;
    scatterMatrix(xs_pca,
                  within_scatter_weights,
                  withinClass, betweenClass, 
                  centroids,
                  wholecentroid);
    cerr << "Starting trace ratio optimization" << endl;
    MatrixXd V, remainV;
    traceRatioOptimization(betweenClass, withinClass, ndim, V, remainV);

#ifdef ORDER_BY_REMOVEDIM
    {
      MatrixXd contracted_withinClass = V.transpose() * withinClass * V;
      MatrixXd contracted_betweenClass = V.transpose() * betweenClass * V;
      MatrixXd rot_in_V(MatrixXd::Identity(ndim, ndim)); 
      
      for(size_t i = 0; i < ndim - 1; ++i) {
        // Extract ndim - i - 1 dimension
        MatrixXd extract, remain;
        traceRatioOptimization(contracted_betweenClass, contracted_withinClass, ndim - i - 1, extract, remain);
        MatrixXd mergeV(ndim - i, ndim - i);
        mergeV.block(0, 0, ndim - i, ndim - i - 1) = extract;
        mergeV.block(0, ndim - i - 1, ndim - i, 1) = remain;
        rot_in_V.block(0, 0, ndim, ndim - i) = (rot_in_V.block(0, 0, ndim, ndim - i) * mergeV).eval();
        contracted_withinClass = (extract.transpose() * contracted_withinClass * extract).eval();
        contracted_betweenClass = (extract.transpose() * contracted_betweenClass * extract).eval();
      }
      cout << rot_in_V << endl;
      V = (V * rot_in_V).eval();
    }
#else
#ifdef ORDER_BY_ONEBYONE
    // One-by-one extraction
    {
      MatrixXd contracted_withinClass = V.transpose() * withinClass * V;
      MatrixXd contracted_betweenClass = V.transpose() * betweenClass * V;
      MatrixXd rot_in_V(MatrixXd::Identity(ndim, ndim)); 
      
      for(size_t i = 0; i < ndim - 1; ++i) {
        // Extract one dimension
        MatrixXd extract, remain;
        traceRatioOptimization(contracted_betweenClass, contracted_withinClass, 1, extract, remain);
        MatrixXd mergeV(ndim - i, ndim - i);
        mergeV.block(0, 0, ndim - i, 1) = extract;
        mergeV.block(0, 1, ndim - i, ndim - i - 1) = remain;
        rot_in_V.block(0, i, ndim, ndim - i) = (rot_in_V.block(0, i, ndim, ndim - i) * mergeV).eval();
        contracted_withinClass = (remain.transpose() * contracted_withinClass * remain).eval();
        contracted_betweenClass = (remain.transpose() * contracted_betweenClass * remain).eval();
      }
      cout << rot_in_V << endl;
      V = (V * rot_in_V).eval();
    }
#else
    // For visualization: reorder V
    for(size_t i = 0; i < ndim / 2; ++i) {
      VectorXd tmp = V.col(i);
      V.col(i) = V.col(ndim - 1 - i);
      V.col(ndim - 1 - i) = tmp;
    }
#endif
#endif

    // Output useful data
    {
      ofstream res("proj_lda.txt");
      for(size_t c = 0; c < xs_pca.size(); ++c) {
        MatrixXd &x = xs_pca[c];
        VectorXd proj(ndim);
        for(size_t i = 0; i < x.cols(); ++i) {
          proj = V.transpose() * (x.col(i) - wholecentroid);
          res.setf(ios::scientific);
          res << setprecision(16);
          res << c << " " << proj.transpose() << endl;
        }
      }
    }

    { 
      ofstream res("vec_pca.txt");
      
      VectorXd out( 3 * ndim );
      for(size_t i = 0; i < nmasked; ++i) {
        for(size_t j = 0; j < ndim; ++j) {
          for(size_t k = 0; k < 3; ++k) {
            out(j * 3 + k) = projmat(i * 3 + k, j);
          }
        }
        res << out.transpose() << endl;
      }
    }

    {
      ofstream res("vec_lda.txt");

      MatrixXd Vreal = projmat * V; // Nx(N-6) * (N-6)xD

      VectorXd out( 3 * ndim );
      for(size_t i = 0; i < nmasked; ++i) {
        for(size_t j = 0; j < ndim; ++j) {
          for(size_t k = 0; k < 3; ++k) {
            out(j * 3 + k) = Vreal(i * 3 + k, j);
          }
        }
        res << out.transpose() << endl;
      }
    }

    if(xs_pca.size() == 2) {
      VectorXd axis = centroids.col(1) - centroids.col(0);
      axis /= axis.norm();
      {
        VectorXd out = projmat * axis;
        ofstream res("vec_direct.txt");
        for(size_t i = 0; i < nmasked; ++i) {
          res << out.segment(i * 3, 3).transpose() << endl;
        }
      }

      ofstream res("proj_direct.txt");
      for(size_t c = 0; c < xs_pca.size(); ++c) {
        MatrixXd &x = xs_pca[c];
        double proj;
        for(size_t i = 0; i < x.cols(); ++i) {
          proj = axis.dot(x.col(i) - wholecentroid);
          res.setf(ios::scientific);
          res << setprecision(16);
          res << c << " " << proj << endl;
        }
      }
    }
  }
  
  return EXIT_SUCCESS;
}







