/* 
Copyright (c) 2013 Shun Sakuraba

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
 */

#include <Eigen/Dense>
#include <Eigen/Eigenvalues>
#include <cassert>

// debug
#include <iostream>
#include <iomanip>
using namespace std;

using namespace Eigen;

void traceRatioOptimization(const MatrixXd &A, const MatrixXd &B, int dim, MatrixXd &V, MatrixXd &Vremain)
{
  assert(A.rows() == B.rows());
  assert(A.cols() == B.cols());
  assert(A.rows() == A.cols());
  size_t N = A.rows();

  if(V.rows() == 0) {
    // Initial guess: use rho = 0
    SelfAdjointEigenSolver<MatrixXd> eigensolver(A);
    V = eigensolver.eigenvectors().block(0, N - dim, N, dim);
  }else{
    assert(V.rows() == A.rows());
    assert(V.cols() == dim);
  }

  double rho;
  double prevrho = -1;
  
  while(true) {
    MatrixXd VtAV = V.transpose() * A * V;
    MatrixXd VtBV = V.transpose() * B * V;
    rho = VtAV.trace() / VtBV.trace();

    cerr.setf(ios::fixed);
    cerr << "Rho': " << setprecision(16) << sqrt(rho) << endl;

    if(fabs(prevrho - rho) < 1e-8) {
      break;
    }

    prevrho = rho;
    
    MatrixXd G = A - rho * B;
    
    SelfAdjointEigenSolver<MatrixXd> eigensolver(G);
    V = eigensolver.eigenvectors().block(0, N - dim, N, dim);
    Vremain = eigensolver.eigenvectors().block(0, 0, N, N - dim);
  }

  return;
}

