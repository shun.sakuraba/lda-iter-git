#ifdef __cplusplus
extern "C" {
#endif

#if 0
} /* indent fix hack */
#endif

void vmdfio_init_(void);
void vmdfio_fini_(void);

void vmdfio_open_(void **handle, const char *fname, int *fnamelen, const char* forcetype, int *status);
void vmdfio_get_natoms_(void **handle, int* natoms);

void vmdfio_read_traj_step_(void **handle, double* xout, double* box, int *natoms_aux, int *status);
void vmdfio_close_traj_(void **handle);
void vmdfio_get_masses_(void **handle, int *natoms, double* masses);

#if 0
{ /* indent fix hack */
#endif

#ifdef __cplusplus
}
#endif
