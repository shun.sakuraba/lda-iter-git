/*
Copyright (c) 2013 Shun Sakuraba

Permission is hereby granted, free of charge, to any person obtaining a copy
 of this software and associated documentation files (the "Software"), to deal
 in the Software without restriction, including without limitation the rights
 to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 copies of the Software, and to permit persons to whom the Software is
 furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
 all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 THE SOFTWARE.
*/

#include <iostream>
#include <iomanip>
#include <fstream>
#include <vector>
#include <Eigen/Dense>

#include "traceRatioOpt.hpp"
#include "scatterMatrix.hpp"

using namespace std;
using namespace Eigen;

int main(int argc, char* argv[])
{
  int N = 1000;
  int dim = 4;
  int classes = 3;

  MatrixXd offsets(MatrixXd::Zero(dim, classes));
  offsets(0, 0) = -2.0;
  offsets(0, 1) = +2.0;
  offsets(2, 2) = +4.0;
  offsets.row(3).setConstant(-2.0);

  VectorXd scale(VectorXd::Constant(dim, 1.0));
  scale(1) = 2.0;
  scale(3) = 0.5;

  vector<MatrixXd> xs(classes);
  for(size_t c = 0; c < classes; ++c) {
    MatrixXd &X = xs[c];
    X = MatrixXd(dim, N);
    VectorXd offset = offsets.col(c);

    for(size_t j = 0; j < N; ++j) {
      for(size_t i = 0; i < dim; ++i) {
	double r1 = (rand() + 1.) / (RAND_MAX + 1.0);
	double r2 = rand() / (RAND_MAX + 1.0);
	
	X(i, j) = sqrt( -2.0 * log(r1) ) * cos(2 * M_PI * r2);
      }
      X.col(j).array() *= scale.array();
      X.col(j) += offset;
    }
  }
  
  MatrixXd withinClass;
  MatrixXd betweenClass;
  MatrixXd centroids;
  VectorXd wholecentroid;
  scatterMatrix(xs, withinClass, betweenClass, centroids, wholecentroid);

  cerr << withinClass << endl;

  MatrixXd V;
  int nseldim = 2;
  traceRatioOptimization(betweenClass, withinClass, nseldim, V);

  {
    ofstream res("proj.txt");
    for(size_t c = 0; c < xs.size(); ++c) {
      MatrixXd &x = xs[c];
      VectorXd proj(nseldim);
      for(size_t i = 0; i < x.cols(); ++i) {
	proj = V.transpose() * x.col(i);
	res.setf(ios::scientific);
	res << setprecision(16);
	res << c << " " << proj.transpose() << endl;
      }
    }
  }

  return 0;
}
